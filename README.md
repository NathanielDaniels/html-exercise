# HTML WEBSITE TEST

## This is a test to see if I can accomplish the following:

###1. Must have horizontal list of links at the top (at least 3) for page navigation 

###2. Must embed a Youtube video of your choosing 

###3. Have at least 2 paragraphs of text with nice spacing between paragraphs. 

###4. Create a table that shows at least 12 images of your favorite fan topic (grab them from Google). Create a colored border around the outside edge of the table, with no border in-between cells. 

###5. Use at least 3 different types of headers
